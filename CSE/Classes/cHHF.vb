﻿' Copyright 2014 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.

Public Class cHHF
    Public KAap As Double                ' Korrected angle from A-P
    Public p As Double                   ' Distance from point A-Bhp (Bhp = Base point from P on route AE)
    Public q As Double                   ' Distance from point E-Bhp (Bhp = Base point from P on route AE)
    Public hp As Double                  ' Ortogonal distance from point P on route AE
End Class
